from django.shortcuts import render, get_object_or_404, redirect
from .forms import PostForm
from .models import Post


def home(request):
    return render(request, 'main/home.html')

def port(request):
    return render(request, 'main/port.html')

def story1(request):
    return render(request, 'main/story1.html')

def homematcool(request):
    return render(request, 'main/homematcool.html')

def matcool(request):
    form = PostForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = PostForm()

    context = {
        'form' : form
    }
    return render(request, 'main/matcool.html', context)

def datamatcool(request):
    data = Post.objects.all()
    context = {
        'data' : data
    }

    return render(request, 'main/datamatcool.html', context)

def detailmatcool(request, my_id):
    data = Post.objects.get(id=my_id)
    context = {
        'data' : data
    }

    return render(request, 'main/detailmatcool.html', context)


def hapusmatcool(request,my_id):
    data = Post.objects.get(id=my_id)
    if request.method == "POST":
        data.delete()
        return redirect('main:datamatcool')
    context = {
        'data' : data
    }
    return render(request, 'main/hapusmatcool.html', context)



