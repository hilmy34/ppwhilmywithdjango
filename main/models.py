from django.db import models

# pilihanTahun = {
    # ('1', '2020/2021'),
    # ('2', '2019/2020'),
    # ('3', '2018/2019'),
    # ('4', '2017/2018'),
    # ('5', '2016/2017'),
    # ('6', '2015/2016'),
    # ('7', '2014/2015'),
    # ('8', '2013/2014'),
    # ('9', '2012/2013')
# }

class Post(models.Model):
    matkul = models.CharField(max_length = 50)
    dosen = models.CharField(max_length = 100)
    sks = models.PositiveIntegerField()
    deskripsi = models.TextField()
    ruangan = models.CharField(max_length = 30)
    tahun = models.CharField(max_length = 10)

    def __str__ (self):
        return self.matkul

