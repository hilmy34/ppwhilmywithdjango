from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    matkul = forms.CharField(required=True)
    dosen = forms.CharField(required=True)
    sks = forms.CharField(required=True)
    deskripsi = forms.CharField(widget=forms.Textarea)
    ruangan = forms.CharField(required=True)
    tahun = forms.CharField(required=True)
 
    matkul.widget.attrs.update({'class':'form-control'})
    dosen.widget.attrs.update({'class':'form-control'})
    sks.widget.attrs.update({'class':'form-control'})
    deskripsi.widget.attrs.update({'class':'form-control'})
    ruangan.widget.attrs.update({'class':'form-control'})
    tahun.widget.attrs.update({'class':'form-control'})


    class Meta:
        model = Post
        fields = ['matkul', 'dosen', 'sks', 'deskripsi', 'ruangan', 'tahun']
 