from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('portfoliocv', views.port, name='port'),
    path('story1', views.story1, name='story1'),
    path('homematcool', views.homematcool, name='homematcool'),
    path('matcool', views.matcool, name='matcool'),
    path('matcool/datamatcool/', views.datamatcool, name='datamatcool'),
    path('matcool/datamatcool/detailmatcool/<int:my_id>/', views.detailmatcool, name='detailmatcool'),
    path('matcool/datamatcool/detailmatcool/<int:my_id>/hapus/', views.hapusmatcool, name='hapusmatcool')
]
